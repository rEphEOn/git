class InvalidMovement(Exception):

    def __init__(self, message):
        self.msg = message

    def __str__(self):
       #return repr('InvalidMovement: {message}'.format(message=self.msg))
       return repr(self.msg)


class GameOver(Exception):

    def __init__(self, message):
        self.msg = message

    def __str__(self):
        #return repr('GameOver: {message}'.format(message=self.msg))
        return repr(self.msg)